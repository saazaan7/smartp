<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Smart Patient</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/smart.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/font-awesome.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="one col-md-5 col-xs-12 col-lg-5">
          <div class="col-md-12 col-xs-12 col-lg-12 banner-top center-block">
            <h2>WELCOME</h2>
          </div>
          <div class="banner">
            <img src="<?php echo base_url()?>assets/images/banner.jpg">
          </div>
        </div>
        <div class="middle col-md-2 col-lg-2"> </div>
        <div class="second col-md-5 col-xs-12 ">
          <div class="adminlogin-logo three col-md-12 ">
            <span class="glyphicon glyphicon-user pull-right" data-toggle="modal"aria-hidden="true" data-target="#myModal"></span>
          </div>
        <!--   =============modal============ -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $title;?></h4>
      </div>
      <div class="modal-body">
        <form class="form-inline" method="post" action="#">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">Username</label>
    <input type="text" class="form-control" name="username" value="Username">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Password</label>
    <input type="password" class="form-control" value="Passwod" name="password">
  </div>
  
  <button type="submit" class="btn btn-default">Sign in</button>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- 
modal end
 -->


          <div class="loginform">
            <form role="form" action="#" method="">
               <div class="form-group">
                
                  <select class="form-control" name="userselect">
                    <option value="" disabled selected>Choose your option</option>
                    <option value="labdoctor">Lab Doctor</option>
                    <option value="departmentdoctor">Department Doctor</option>
                    <option value="registrant">Registrant</option>
                  </select>
              </div>
              <div class="form-group">
               
                 <input type="email" class="form-control" id="email" placeholder="Enter email">
              </div>
              <div class="form-group">
               
                <input type="password" class="form-control" id="pwd" placeholder="Enter password">
              </div>
              <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
              </div>
              <button type="submit" class="btn btn-default loginbtn">Submit <i class="fa fa-sign-in" aria-hidden="true"></i></button>
            </form>
          </div>
        </div>
      </div> <!-- row ends -->
    </div> <!-- container-fluid end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
  </body>
</html>