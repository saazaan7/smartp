<div class="col-md-9">
    <div class=""><button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addModal">Add Doctors</button></div>

    <h2>Welcome to Dashboard</h2>
    <div class="alert alert-<?php echo $this->session->flashdata('class'); ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $this->session->flashdata('msg'); ?>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Doctor Category</div>

        <table class="table table-responsive table-hover">
            <thead class="thead-inverse">
            <th>ID</th>
            <th>Name</th>
            <th>Special In</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Working Hospital</th>
            <th>Edit/Delete</th>

            </thead>
            <tbody>
                <?php foreach ($categories as $c): ?>
                    <tr>
                        <td><?php echo $c['id']; ?></td>
                        <td><?php echo $c['name']; ?></td>
                        <td><?php echo $c['special_in']; ?></td>
                        <td><?php echo $c['email']; ?></td>
                        <td><?php echo $c['phone']; ?></td>
                        <td><?php echo $c['working_hospital']; ?></td>
                        <td>
                            <a href="#"><i class="fa fa-pencil-square-o" data-toggle="modal" data-target="#editModal"aria-hidden="true"></i></a>
                            <a href="<?php echo base_url();?>login/deleteDoctor/<?php echo $c['id'];?>" onclick="return confirm('Are you sure you want to delete this?');"> &nbsp <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!--   =============modal============ -->

    <!-- Modal to edit -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="" name="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Special In</label>
                            <input type="text" class="form-control" id="specialin" placeholder="" name="special_in">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="" name="email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Phone</label>
                            <input type="text" class="form-control" id="phone" placeholder="" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Working Hospital</label>
                            <input type="text" class="form-control" id="workinghospital" placeholder="" name="working_hospital">
                        </div>
                        <button type="submit" class="btn btn-default" name="doctor-add">ADD+</button>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->

    <!-- Modal to add -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Add Doctors</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo base_url(); ?>login/register_doctor">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Doctor name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Special In</label>
                            <input type="text" class="form-control" name="specialin" placeholder="Special In">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Working Hospital</label>
                            <input type="text" class="form-control" name="workinghospital" placeholder="Working Hospital">
                        </div>
                        <button type="submit" class="btn btn-default">ADD+</button>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->



</div>
</div>
</div>
