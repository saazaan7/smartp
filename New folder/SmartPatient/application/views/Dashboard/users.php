<div class="col-md-9">
    <div class=""><button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addModal">Add Users</button></div>

    <h2>Welcome to Dashboard</h2>
    <div class="alert alert-<?php echo $this->session->flashdata('class'); ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <?php echo $this->session->flashdata('msg'); ?>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Users panel</div>
        <table class="table table-responsive table-hover">
            <thead class="thead-inverse">
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Age</th>
            <th>Address</th>
            <th>Disease</th>
            <th>Created Time</th>
            <th>Edit/Delete</th>

            </thead>
            <tbody>
                <?php foreach ($categories as $c): ?>
                    <tr>
                        <td><?php echo $c['id']; ?></td>
                        <td><?php echo $c['name']; ?></td>
                        <td><?php echo $c['gender']; ?></td>
                        <td><?php echo $c['age']; ?></td>
                        <td><?php echo $c['address']; ?></td>
                        <td><?php echo $c['disease']; ?></td>
                        <td><?php echo $c['datetime']; ?></td>
                        <td>
                            <a href="#"><i class="fa fa-pencil-square-o" data-toggle="modal" data-target="#editModal"aria-hidden="true"></i></a>
                            <a href=<?php echo base_url();?>login/deleteUser/<?php echo $c['id'];?>" onclick="return confirm('Are you sure you want to delete this item?');"> &nbsp <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>

        </table>
    </div>
    <!--   =============modal============ -->

    <!-- Modal to edit -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Gender</label>
                            <input type="text" class="form-control" name="gender" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Age</label>
                            <input type="text" class="form-control" name="age" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Address</label>
                            <input type="text" class="form-control" name="address" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Disease</label>
                            <input type="text" class="form-control" name="disease" placeholder="">
                        </div>

                        <button type="submit" class="btn btn-default" name="user-submit">ADD+</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->

    <!-- Modal to add -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Add Users</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url(); ?>login/register_user" method="post">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Gender</label>
                            <input type="text" class="form-control" name="gender" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Age</label>
                            <input type="text" class="form-control" name="age" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Address</label>
                            <input type="text" class="form-control" name="address" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Disease</label>
                            <input type="text" class="form-control" name="disease" placeholder="">
                        </div>
                        <button type="submit" name="user-submit" class="btn btn-default">ADD+</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->



</div>
</div>
</div>