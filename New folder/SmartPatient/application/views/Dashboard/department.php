
<div class="col-md-9">
    <h2>Welcome to Dashboard</h2>
    <h3>This is department Category</h3>
    <div class="alert alert-<?php echo $this->session->flashdata('class');?> alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   <?php echo $this->session->flashdata('msg');?>
    </div>
    <table class="table table-responsive table-hover">
        <thead class="thead-inverse">
        <th>ID</th>
        <th>Category</th>
        <th>Info</th>
        <th>Edit/Delete</th>
        </thead>
        <tbody>
            <?php foreach($categories as $c):?>
        <tr>
            <td><?php echo $c['id'];?></td>
            <td><?php echo $c['category'];?></td>
            <td><?php echo $c['info'];?></td>
            <td><a href="#"><i class="fa fa-pencil-square-o" data-toggle="modal" data-target="#editModal"aria-hidden="true"></i></a>
                <a href="<?php echo base_url();?>login/deleteDepartment/<?php echo $c['id'];?>" onclick="return confirm('Are you sure you want to delete this item?');"> &nbsp <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
        </tr>
        <?php endforeach;?>
        </tbody>
        
    </table>
    <div class=""><button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addModal">Add +</button></div>

    <!--   =============modal============ -->

    <!-- Modal to edit -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">ID</label>
                            <input type="number" class="form-control" id="editid" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Category</label>
                            <input type="text" class="form-control" id="category" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Info</label>
                            <input type="text" class="form-control" id="info" placeholder="">
                        </div>

                        <button type="" class="btn btn-default">Edit</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->

    <!-- Modal to add -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Add Category</h4>
                </div>
                <div class="modal-body">

                    <form action="<?php echo base_url(); ?>login/register_department" method="post">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Category</label>
                            <input name="category" type="text" class="form-control"  placeholder="Category name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Info</label>
                            <input name="info" type="text" class="form-control" placeholder="Enter info about this category">
                        </div>

                        <button type="submit" name="add" class="btn btn-default">ADD+</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- 
    modal end
    -->



</div>
</div>
</div>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

</body>
</html>