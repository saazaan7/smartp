<?php

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function login($input) {

        $username = $input['username'];
        $password = $input['password'];

        $query = $this->db->query("SELECT * FROM superadmin WHERE username='$username' AND password='$password'");
        $data = $query->row();
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function register_department($input) {

        $this->db->insert('department_db', $input);
        $this->session->set_flashdata('msg', "Inserted Successfully");
    }

    public function register_doctor($input) {

        $this->db->insert('doctor_db', $input);
        $this->session->set_flashdata('msg', "Inserted Successfully");
    }

    public function register_user($input) {
        $input['datetime'] = date('Y/m/d:h-i-s');
        $this->db->insert('user_db', $input);
        $this->session->set_flashdata('msg', "Inserted Successfully");
    }

    public function getAllDepartment() {
        $this->db->select('*')->from('department_db');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllDoctor() {
        $this->db->select('*')->from('doctor_db');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllUser() {
        $this->db->select('*')->from('user_db');
        $query = $this->db->get();
        return $query->result_array();
    }
     public function deleteDoctor($id){
        $this->db->query("DELETE FROM doctor_db WHERE id='$id'");
        $this->session->set_flashdata('msg',"Deletion successfull");
    }
     public function deleteDepartment($id){
        $this->db->query("DELETE FROM department_db WHERE id='$id'");
        $this->session->set_flashdata('msg',"Deletion successfull");
    }
     public function deleteUser($id){
        $this->db->query("DELETE FROM user_db WHERE id='$id'");
        $this->session->set_flashdata('msg',"Deletion successfull");
    }

}
?>

