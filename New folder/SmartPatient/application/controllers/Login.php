<?php

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_model');
        // $this->load->library('form_validation');
    }
//    public function index() {
//        $this->getAll();
//    }

    public function login() {
        $data['title'] = "Signup Form";
        if ($_POST) {
            $input = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
            );
            $res = $this->Login_model->login($input);
            if ($res) {
                $sess_data = array(
                    'username' => $res->username,
                    'loggedIn' => 1
                );
                $this->session->set_userdata('session_data', $sess_data);
                redirect('login/dashboard');
            } else {
                echo "login unsuccessfull";
            }
        } else {
            $this->load->view('home/home', $data);
        }
    }

    public function register_department() {
        if ($_POST) {
            $input = array(
                'category' => $this->input->post('category'),
                'info' => $this->input->post('info'),
            );


            $this->Login_model->register_department($input);
            redirect('login/departments');
        } else {
            echo"fuck u neeraj";
        }
    }

    public function register_doctor() {

        if ($_POST) {
            $input = array(
                'name' => $this->input->post('name'),
                'special_in' => $this->input->post('specialin'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'working_hospital' => $this->input->post('workinghospital'),
            );


            $this->Login_model->register_doctor($input);
            redirect('login/doctors');
        }
    }
    public function register_user() {

        if ($_POST) {
            $input = array(
                'name' => $this->input->post('name'),
                'gender' => $this->input->post('gender'),
                'age' => $this->input->post('age'),
                'address' => $this->input->post('address'),
                'disease' => $this->input->post('disease'),
                
            );


            $this->Login_model->register_user($input);
            redirect('login/users');
        }
    }

    public function dashboard() {
//       print_r($this->session->all_userdata()); 
//         exit;
        // check if loggedin
        //$this->isLoggedIn();
        //display dashboard
        // $data['users'] = $this->user_model->getAll();
        // $data['title'] = "DAshboard";
        //$this->load->view('base/header', $data);
        $this->load->view('base/header');
        $this->load->view('Dashboard/dashboard');
    }

    public function departments() {
        $data['categories'] = $this->Login_model->getAllDepartment();
        $this->load->view('base/header',$data);
        $this->load->view('Dashboard/department');
        $this->load->view('base/footer');
    }

    public function doctors() {
        $data['categories'] = $this->Login_model->getAllDoctor();
        $this->load->view('base/header',$data);
        $this->load->view('Dashboard/doctor');
        $this->load->view('base/footer');
    }

    public function users() {
        $data['categories'] = $this->Login_model->getAllUser();
        $this->load->view('base/header',$data);
        $this->load->view('Dashboard/users');
        $this->load->view('base/footer');
    }

    public function reports() {
        $this->load->view('base/header');
        $this->load->view('Dashboard/reports');
        $this->load->view('base/footer');
    }

    public function hospital() {
        $this->load->view('base/header');
        $this->load->view('Dashboard/hospital');
        $this->load->view('base/footer');
    }

    public function disease() {
        $this->load->view('base/header');
        $this->load->view('Dashboard/disease');
        $this->load->view('base/footer');
    }

    public function medicine() {
        $this->load->view('base/header');
        $this->load->view('Dashboard/medicine');
        $this->load->view('base/footer');
    }


 private function getAll() {
        
        $data['categories'] = $this->Login_model->getAllDepartment();
        $this->load->view('base/header', $data);
        $this->load->view('Dashboard/department');
        $this->load->view('base/footer');
    }
    public function deleteDoctor($id) {
       
        $this->Login_model->deleteDoctor($id);
        redirect('login/doctors');
    }
    public function deleteDepartment($id) {
       
        $this->Login_model->deleteDepartment($id);
        redirect('login/departments');
    }
    public function deleteUser($id) {
       
        $this->Login_model->deleteUser($id);
        redirect('login/users');
    }
}
    
?>



