-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2016 at 04:33 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_patient_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `department_db`
--

CREATE TABLE `department_db` (
  `id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `info` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_db`
--

INSERT INTO `department_db` (`id`, `category`, `info`) VALUES
(1, 'Culpa eiusmod dolorum non quae laborum nulla obcae', 'In quia quae ut quos'),
(2, 'Ea exercitationem molestias et consequatur porro i', 'Dolorem et beatae necessitatibus consequatur eius placeat dolor consectetur iste pariatur Earum sed explicabo Dolor exercitationem nihil vel');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_db`
--

CREATE TABLE `doctor_db` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `special_in` varchar(70) NOT NULL,
  `email` varchar(70) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `working_hospital` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_db`
--

INSERT INTO `doctor_db` (`id`, `name`, `special_in`, `email`, `phone`, `working_hospital`) VALUES
(8, 'Amal Coleman', 'Omnis odit cum eveniet porro quis', 'jupesah@yahoo.com', '+527-41-9480825', 'Cum ut omnis ullamco mollit aliqua Sunt ipsum enim omnis magni molestias voluptatem Ea');

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE `superadmin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`username`, `password`) VALUES
('root', 'root');

-- --------------------------------------------------------

--
-- Table structure for table `user_db`
--

CREATE TABLE `user_db` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` varchar(2) NOT NULL,
  `address` varchar(100) NOT NULL,
  `disease` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_db`
--

INSERT INTO `user_db` (`id`, `name`, `gender`, `age`, `address`, `disease`, `datetime`) VALUES
(2, 'Martina Vincent', 'Praesentiu', '88', 'Cupiditate et corrupti cillum ex exercitation aperiam perferendis debitis doloribus in quia soluta d', 'Ipsum facilis perspiciatis ipsum commodi eu', '2016-06-24 07:06:08'),
(3, 'Benjamin Robertson', 'Qui non is', '22', 'Ad dolore amet dolorem obcaecati odio qui officia assumenda voluptas velit molestias inventore dolor', 'Dolore commodi beatae aut dolorem alias', '2016-06-25 03:31:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department_db`
--
ALTER TABLE `department_db`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_db`
--
ALTER TABLE `doctor_db`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `user_db`
--
ALTER TABLE `user_db`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department_db`
--
ALTER TABLE `department_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doctor_db`
--
ALTER TABLE `doctor_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_db`
--
ALTER TABLE `user_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
